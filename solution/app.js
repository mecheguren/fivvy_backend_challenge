const mongoose = require('mongoose')
const dotenv = require('dotenv')
const express = require('express')

const disclaimerRouter = require('./routes/disclaimerRoutes')
const acceptanceRouter = require('./routes/acceptanceRoutes')

dotenv.config({ path: './.env' })

const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => console.log('DB connection successful!'));

const port = process.env.PORT || 3000
const app = express()

app.use(express.json({ limit: '10kb' }))

app.use('/api/v1/disclaimers', disclaimerRouter)
app.use('/api/v1/acceptances', acceptanceRouter)

app.listen(port, () => {
  console.log(`App running on port ${port}...`)
})

module.exports = app