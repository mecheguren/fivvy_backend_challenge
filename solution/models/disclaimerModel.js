const mongoose = require('mongoose');

const disclaimerSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Disclaimer must have name']
    },
    text: {
      type: String,
      required: [true, 'Disclaimer must have text']
    },
    version: {
      type: Number,
      required: [true, 'Disclaimer must have version']
    },
    createAt: {
      type: Date,
      default: Date.now(),
    },
    updateAt: {
      type: Date,
      default: Date.now(),
    },
  }
);

const Disclaimer = mongoose.model('Disclaimer', disclaimerSchema);

module.exports = Disclaimer;
