const mongoose = require('mongoose');

const acceptanceSchema = new mongoose.Schema(
  {
    disclaimer_id: {
      type: mongoose.Schema.ObjectId,
      ref: 'Disclaimer',
      required: [true, 'Acceptance must belong to a disclaimer.']
    },
    user_id: {
      type: String,
      required: [true, 'Acceptance must have user_id']
    },
    createAt: {
      type: Date,
      default: Date.now(),
    }
  }
);

const Acceptance = mongoose.model('Acceptance', acceptanceSchema);

module.exports = Acceptance;
