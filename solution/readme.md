# How to run this project:

1° Run the following command to install package.json contents:<br />
npm install.<br />
2° Create a .env file with parameters (Use .env.example as a guideline)<br />
3a° Run the following command to execute the application without docker:<br />
npm start\*\*
3b° Run the following command to execute the application with docker:<br />
docker compose up -d\*\*

# How to run tests (without docker):

1° npm run test (I used jest for testing)<br />

# Project structure:

--Models: contains DB entities. I used MongoDB as DB and Mongoose as ORM.<br />
--Routes: Routes definitions.<br />
--Controllers: Routes implementations.<br />
--Tests: Includes tests.<br />
--app.js: Starting point of the application.<br />

# Endpoints:

--GET all disclaimers: http://IP Address:PORT/api/v1/disclaimers<br />
(You can filter by text (for example: text=customtext) by using query string parameters).<br />
--POST create disclaimer: http://IP Address:PORT/api/v1/disclaimers<br />
(Body must include name, text, and version)<br />
--PATCH update disclaimer: http://IP Address:PORT/api/v1/disclaimers<br />
(Body can include name, text, and version)<br />
--DELETE remove disclaimer: http://IP Address:PORT/api/v1/disclaimers/:disclaimer_id<br />

--GET all acceptances: http://IP Address:PORT/api/v1/acceptances<br />
(You can filter by user_id (for example: user_id=customuser1) by using query string parameters).<br />
--POST create acceptance: http://IP Address:PORT/api/v1/acceptances<br />
(Body must include disclaimer_id, and user_id)<br />