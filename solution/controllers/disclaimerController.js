const Disclaimer = require('../models/disclaimerModel');

exports.getAllDisclaimers = async (req, res) => {
  try {
    const disclaimersDB = await Disclaimer.find({}).select('-__v')

    const disclaimers =
    req.query.text !== undefined
      ? disclaimersDB.filter((disclaimer) => disclaimer.text === req.query.text)
      : disclaimersDB

    res.status(200).json({
      status: 'success',
      results: disclaimers.length,
      data: {
        disclaimers
      }
    });
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: err
    });
  }
};

exports.createDisclaimer = async (req, res) => {
  try {
    const newDisclaimer = await Disclaimer.create(req.body)

    res.status(201).json({
      status: 'success',
      data: {
        disclaimer: {_id: newDisclaimer._id, name: newDisclaimer.name, text: newDisclaimer.text, version: newDisclaimer.version}
      }
    });
  } catch (err) {
    res.status(400).json({
      status: 'fail',
      message: err
    });
  }
};

exports.updateDisclaimer = async (req, res) => {
  try {
    req.body.updateAt = Date.now()
    const disclaimer = await Disclaimer.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true
    });

    res.status(200).json({
      status: 'success',
      data: {
        disclaimer: {_id: disclaimer._id, name: disclaimer.name, text: disclaimer.text, version: disclaimer.version}
      }
    });
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: err
    });
  }
};

exports.deleteDisclaimer = async (req, res) => {
  try {
    await Disclaimer.findByIdAndDelete(req.params.id)

    res.status(204).json({
      status: 'success',
      data: null
    });
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: err
    });
  }
};
