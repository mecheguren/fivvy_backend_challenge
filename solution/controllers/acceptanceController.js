const Acceptance = require('../models/acceptanceModel');

exports.getAllAcceptances = async (req, res) => {
  try {
    const acceptancesDB = await Acceptance.find({}).select('-__v')

    const acceptances =
    req.query.user_id !== undefined
      ? acceptancesDB.filter((acceptance) => acceptance.user_id === req.query.user_id)
      : acceptancesDB

    res.status(200).json({
      status: 'success',
      results: acceptances.length,
      data: {
        acceptances
      }
    });
  } catch (err) {
    res.status(404).json({
      status: 'fail',
      message: err
    });
  }
};

exports.createAcceptance = async (req, res) => {
  try {
    const newAcceptance = await Acceptance.create(req.body)

    res.status(201).json({
      status: 'success',
      data: {
        disclaimer: {disclaimer_id: newAcceptance.disclaimer_id, user_id: newAcceptance.user_id, createAt: newAcceptance.createAt}
      }
    });
  } catch (err) {
    res.status(400).json({
      status: 'fail',
      message: err
    });
  }
};