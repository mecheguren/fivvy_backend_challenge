const mongoose = require('mongoose')
const request = require('supertest')
const app = require('../app')
const dotenv = require('dotenv')

dotenv.config({ path: './.env' })

const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);

beforeEach(async () => {
  await mongoose.connect(DB);
});

afterEach(async () => {
  await mongoose.connection.close()
});

describe('POST api/v1/disclaimers', () => {
  it('should create a disclaimer', async () => {
    const res = await request(app).post("/api/v1/disclaimers").send({
      name: "Disclaimer 10 name",
      text: "Disclaimer 10 text",
      version: 1
    })
    expect(res.statusCode).toBe(201);
  });
});

describe('GET api/v1/disclaimers', () => {
  it('should get all disclaimers', async () => {
    const res = await request(app).get("/api/v1/disclaimers");
    expect(res.statusCode).toBe(200);
    expect(res.body.results).toBeGreaterThan(0);
  });
});

describe('POST api/v1/acceptances', () => {
  it('should create an acceptance', async () => {
    const res = await request(app).post("/api/v1/acceptances").send({
      disclaimer_id: "64cabb1d187a0a00185c17f4",
      user_id: "Acceptance User 1"
    })
    expect(res.statusCode).toBe(201);
  });
});

describe('GET api/v1/acceptances', () => {
  it('should get all acceptances', async () => {
    const res = await request(app).get("/api/v1/acceptances");
    expect(res.statusCode).toBe(200);
    expect(res.body.results).toBeGreaterThan(0);
  });
});