const express = require('express')

const acceptanceController = require('../controllers/acceptanceController')

const router = express.Router()

router
  .route('/')
  .get(acceptanceController.getAllAcceptances)
  .post(acceptanceController.createAcceptance)

module.exports = router
