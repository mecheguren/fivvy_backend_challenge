const express = require('express')

const disclaimerController = require('../controllers/disclaimerController')

const router = express.Router()

router
  .route('/')
  .get(disclaimerController.getAllDisclaimers)
  .post(disclaimerController.createDisclaimer)

router
  .route('/:id')
  .patch(disclaimerController.updateDisclaimer)
  .delete(disclaimerController.deleteDisclaimer)

module.exports = router
